package com.devcamp.midtest0120.stringapi.controller;

import java.util.HashSet;
import java.util.Set;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class StringController {
    @GetMapping("/string-reverse")
    public String getReverseString(@RequestParam(required = true) String str) {
        // Cách 1:
        /* StringBuilder strBuilder = new StringBuilder();
        char[] strChars = str.toCharArray();
        for (int i = strChars.length - 1; i >= 0; i--) {
            strBuilder.append(strChars[i]);
        }
        return strBuilder.toString(); */
        // Cách 2:
        StringBuilder stringBuilder = new StringBuilder(str);
        return stringBuilder.reverse().toString();
    }
    @GetMapping("/is-palindrome")
    public String isPalindrome(@RequestParam(required = true) String str) {
        String result = "";
        StringBuilder stringBuilder = new StringBuilder(str);
        if(str.equals(stringBuilder.reverse().toString()) == true) {
            result = "Đây là chuỗi palindrome";
        }
        else {
            result = "Đây không phải là chuỗi palindrome";
        }
        return result;
    }
    @GetMapping("/remove-duplicate-char")
    public String removeDuplicateChar(@RequestParam(required = true) String str) {
        Set<Character> charsPresent = new HashSet<>();
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i < str.length(); i++) {
            if (!charsPresent.contains(str.charAt(i))) {
                stringBuilder.append(str.charAt(i));
                charsPresent.add(str.charAt(i));
            }
        }
        return stringBuilder.toString();
    }
    @GetMapping("/string-concat")
    public String getStringsConcatenation(
        @RequestParam(required = true) String str1, 
        @RequestParam(required = true) String str2
    ) {
        String result = null;
        if(str1.length() == str2.length()) {
            result = str1 + str2;
        }    
        else if(str1.length() > str2.length()) {
            int diff = str1.length() - str2.length();
            result = str1.substring(diff, str1.length()) + str2;
        }
        else {
            int diff = str2.length() - str1.length();
            result = str1 + str2.substring(diff, str2.length());
        }
        return result;
    }
}
